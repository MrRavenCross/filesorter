﻿using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Filesorter.Model
{
    class MoveDirectoryFile
    {

        /// <summary>
        /// Call this to move an AdvancedDirectory
        /// </summary>
        /// <param name="dir">The AdvancedDirectory that is going to be moved</param>
        public static void Move(AdvancedDirectory dir)
        {
            // TODO: Seperate Drive methods so that it uses moveto when the CurrentDirectory and DesiredDirectory is the same
            // TODO: and if the are not it should use copy/delete.
            if (dir.CurrentDirectory.Split(':')[0] == dir.DesiredDirectory.Split(':')[0])
                moveSameRoot(dir);
            else
                moveDifferentRoot(dir);
        }

        private static void moveSameRoot(AdvancedDirectory dir)
        {
            if (!Directory.Exists(dir.DesiredDirectory))
                Directory.CreateDirectory(dir.DesiredDirectory);

            var dirInfo = new DirectoryInfo(dir.CurrentDirectory);
            var subDir = dirInfo.GetDirectories();
            var actionList = new List<Action>();

            if (subDir.Length > 0)
            {
                foreach (var sub in subDir)
                {
                    if (findCollectionArraySize(dir, sub.Name))
                    {
                        var dirinfo = new DirectoryInfo(Path.Combine(dir.CurrentDirectory, sub.Name));
                        Action action = () => dirinfo.MoveTo(Path.Combine(dir.DesiredDirectory, sub.Name));
                        actionList.Add(action);

                        var dirPath = Path.Combine(dir.CurrentDirectory, sub.Name);
                        DirectoryInfo info = new DirectoryInfo(dirPath);
                    }
                }
            }
            moveFilesToActionsSameRoot(dirInfo, dir, ref actionList);
            executeActionsGeneric(actionList, null);
        }

        private static void moveFilesToActionsSameRoot(DirectoryInfo dirInfo, AdvancedDirectory dir, ref List<Action> actionList)
        {
            FileInfo[] filesInfo = dirInfo.GetFiles();
            if (filesInfo.Length > 0)
            {
                foreach (var file in filesInfo)
                {
                    // TODO: Catch IO exception for if the file/directory already exist
                    if (findCollectionArraySize(dir, file.Name))
                    {
                        try
                        {
                            var path = Path.Combine(dir.DesiredDirectory, file.Name);                           
                            Action action = () => file.MoveTo(path);
                            actionList.Add(action);
                        }
                        catch (IOException ioException)
                        {

                            if (ioException.Message.Contains("File already exist"))
                                break;

                            throw;
                        }
                    }
                }
            }
        }
        private static void moveDifferentRoot(AdvancedDirectory dir)
        {
            if (!Directory.Exists(dir.DesiredDirectory))
                Directory.CreateDirectory(dir.DesiredDirectory);

            var dirInfo = new DirectoryInfo(dir.CurrentDirectory);
            var subDir = dirInfo.GetDirectories();
            var actionListCopyFile = new Dictionary<string, Action>();
            var actionsListDeleteFile = new Dictionary<string, Action>();
            var actionListCopyDir = new List<Action>();
            var actionsListDeleteDir = new List<Action>();

            if (subDir.Length > 0)
            {
                foreach (var sub in subDir)
                {
                    if (findCollectionArraySize(dir, sub.Name))
                    {
                        if (sub.GetDirectories().Any())
                        {
                            foreach (var subSubDir in sub.GetDirectories())
                            {
                                var innerPath = Path.Combine(dir.DesiredDirectory, sub.Name + @"\" + subSubDir.Name);
                                Action actionSub = () => Directory.CreateDirectory(innerPath);
                                actionListCopyDir.Add(actionSub);

                                moveFilesToActionsSubDirs(subSubDir, dir, ref actionListCopyFile, ref actionsListDeleteFile);
                                Action actionDeleteSub =
                                    () => Directory.Delete(Path.Combine(dir.CurrentDirectory, sub.Name + @"\" + subSubDir.Name));
                                actionsListDeleteDir.Add(actionDeleteSub);
                            }
                        }
                        var path = Path.Combine(dir.DesiredDirectory, sub.Name);
                        Action action = () => Directory.CreateDirectory(path);
                        actionListCopyDir.Add(action);

                        moveFilesToActionsDiffenerentRoot(sub, dir, ref actionListCopyFile, ref actionsListDeleteFile);
                        var dirPath = Path.Combine(dir.CurrentDirectory, sub.Name);
                        moveRestFilesDifferentRoot(dirPath, path, ref actionListCopyFile, ref actionsListDeleteFile);
                        Action actionDelete = () => Directory.Delete(dirPath);
                        actionsListDeleteDir.Add(actionDelete);
                        
                        //actionListCopyDir.Clear();
                        //actionsListDeleteDir.Clear();
                        //actionListCopyFile.Clear();
                        //actionsListDeleteFile.Clear();
                    }
                }
            }
            moveFilesToActionsDiffenerentRoot(dirInfo, dir, ref actionListCopyFile, ref actionsListDeleteFile);
            var tempListCopyFile = new List<Action>();
            var tempListDeleteFile = new List<Action>();
            foreach (var action in actionListCopyFile)
            {
                tempListCopyFile.Add(action.Value);
            }
            foreach (var action in actionsListDeleteFile)
            {
                tempListDeleteFile.Add(action.Value);
            }
            executeActionsGeneric(tempListCopyFile, actionListCopyDir);
            executeActionsGeneric(tempListDeleteFile, actionsListDeleteDir);
        }

        private static void moveRestFilesDifferentRoot(string dirPath, string path, ref Dictionary<string, Action> actionListCopyFile,
            ref Dictionary<string, Action> actionsListDeleteFile)
        {
            DirectoryInfo info = new DirectoryInfo(dirPath);

            var restFiles = info.GetFiles();
            if (restFiles.Any())
            {
                foreach (var file in restFiles)
                {
                    try
                    {
                        if (!File.Exists(Path.Combine(path, file.Name)))
                        {                            
                            if (!actionListCopyFile.ContainsKey(file.Name))
                            {
                                var fileName = Path.Combine(path, file.Name);
                                Action restAction = () => file.CopyTo(fileName);
                                actionListCopyFile.Add(file.Name, restAction);

                                Action restActionDelete = () => file.Delete();
                                actionsListDeleteFile.Add(file.Name, restActionDelete);
                            }
                        }
                    }
                    catch (IOException)
                    {
                        //Shit happens sometimes
                    }
                }
            }

        }

        private static void executeActionsGeneric<T>(T actionListCopy, T actionsListDelete) where T : List<Action>
        {
            try
            {
                if (actionListCopy.Count > 0)
                {
                    Parallel.Invoke(actionListCopy.ToArray());

                    if (actionsListDelete.Count > 0)
                        Parallel.Invoke(actionsListDelete.ToArray());
                }
            }
            catch (IOException ioEx)
            {
                //Shit happens
            }
            catch (NullReferenceException nullref)
            {
                //It's supposed to happen with the actionListDelete when it's the method SameRoot, since it pass' a null
            }           

        }

        private static void moveFilesToActionsDiffenerentRoot(DirectoryInfo dirInfo, AdvancedDirectory dir, ref Dictionary<string, Action> actionListCopy, ref Dictionary<string, Action> actionsListDelete)
        {
            FileInfo[] filesInfo = dirInfo.GetFiles();
            if (filesInfo.Length > 0)
            {
                foreach (var file in filesInfo)
                {
                    // TODO: Catch IO exception for if the file/directory already exist
                    if (findCollectionArraySize(dir, file.Name))
                    {
                        try
                        {
                            string path = "";
                            if (file.Name.Contains(dir.CollectionName))
                            {
                                path = Path.Combine(dir.DesiredDirectory + @"\" + dirInfo.Name, file.Name);
                                if (!Directory.Exists(Path.Combine(dir.DesiredDirectory + @"\" + dirInfo.Name)))
                                    Directory.CreateDirectory(dir.DesiredDirectory + @"\" + dirInfo.Name);

                            }
                            else
                                path = Path.Combine(dir.DesiredDirectory, file.Name);

                            Action action = () => file.CopyTo(path);
                            actionListCopy.Add(file.Name, action);
                            Action actionDelete = () => file.Delete();
                            actionsListDelete.Add(file.Name, actionDelete);
                        }
                        catch (IOException ioException)
                        {

                            if (ioException.Message.Contains("File already exist"))
                                break;

                            throw;
                        }
                    }
                }
            }
        }


        private static void moveFilesToActionsSubDirs(DirectoryInfo dirInfo, AdvancedDirectory dir, ref Dictionary<string, Action> actionListCopy, ref Dictionary<string, Action> actionsListDelete)
        {
            FileInfo[] filesInfo = dirInfo.GetFiles();
            if (filesInfo.Length > 0)
            {
                foreach (var file in filesInfo)
                {
                    // TODO: Catch IO exception for if the file/directory already exist
                    if (findCollectionArraySize(dir, file.Name))
                    {
                        try
                        {
                            string path = Path.Combine(dir.DesiredDirectory + @"\" + dirInfo.Name, file.Name);
                            if (!Directory.Exists(Path.Combine(dir.DesiredDirectory + @"\" + dirInfo.Name)))
                                Directory.CreateDirectory(dir.DesiredDirectory + @"\" + dirInfo.Name);

                            Action action = () => file.CopyTo(path);
                            actionListCopy.Add(file.Name,action);
                            Action actionDelete = () => file.Delete();
                            actionsListDelete.Add(file.Name,actionDelete);
                        }
                        catch (IOException ioException)
                        {

                            if (ioException.Message.Contains("File already exist"))
                                break;

                            throw;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Call this to determine if a directory/file contains the full name.
        /// </summary>
        /// <param name="dir">The AdvancedDirectory that is going to be moved</param>
        /// <param name="name">A file or a subdirectorys name</param>
        /// <returns>A bool, if it's true the directory/file contains the AdvancedDirectory name, if false it does not</returns>
        private static bool findCollectionArraySize(AdvancedDirectory dir, string name)
        {
            bool g = false;
            var tempName = dir.CollectionName.ToUpper();
            string[] tempCollectionName;
            if (tempName.Contains("THE"))
            {
                tempCollectionName = tempName.Replace("THE", "").ToUpper().Split(' ');
            }
            else
                tempCollectionName = dir.CollectionName.ToUpper().Split(' ');


            List<string> collectionName = tempCollectionName.ToList();         
            foreach (string t in collectionName)
            {
                if (!string.IsNullOrWhiteSpace(t))
                {
                    if (name.ToUpper().Contains(t))
                    {
                        g = true;
                    }
                }
            }
            return g;
        }
    }
}

