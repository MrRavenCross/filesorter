﻿using System;
using System.Windows.Forms;
using Filesorter.Controller;
using Filesorter.Model;

namespace Filesorter.View
{
    public partial class Form1 : Form
    {
        private readonly SystemController sysController;

        public Form1()
        {
            InitializeComponent();

            sysController = SystemController.GetViewModel;
            reBindDataGridView();
            FormClosing += Form1_FormClosing;
        }

        private void reBindDataGridView()
        {
            var source = new BindingSource(sysController.CollectionCatalog, null);
            listBoxName.DataSource = source;
            listBoxName.DisplayMember = "CollectionName";
            listBoxCurrentDir.DataSource = source;
            listBoxCurrentDir.DisplayMember = "DesiredDirectory";
            listBoxNumberOfFiles.DataSource = source;
            listBoxNumberOfFiles.DisplayMember = "NumberOfFiles";
            listBox4TotalSize.DataSource = source;
            listBox4TotalSize.DisplayMember = "GetSizeAppropriate";
            textBoxName.Clear();
            textBoxStart.Clear();
            textBoxEnd.Clear();
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            folderBrowserDialogStart.ShowDialog();
            textBoxStart.Text = folderBrowserDialogStart.SelectedPath;
        }

        private void buttonEnd_Click(object sender, EventArgs e)
        {
            folderBrowserDialogEnd.ShowDialog();
            textBoxEnd.Text = folderBrowserDialogEnd.SelectedPath;
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            try
            {
                setViewModelStrings();
                sysController.AddMethod();
                reBindDataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                sysController.DeleteMethod();
                reBindDataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonSort_Click(object sender, EventArgs e)
        {
            try
            {
                sysController.SortMethod();
                reBindDataGridView();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    MessageBox.Show(ex.InnerException.Message);
                else
                    MessageBox.Show(ex.Message);
            }

        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                string valueName = "";
                string valueCurrentDir = "";
                string valueDesDir = "";
                var result = DialogPrompt.Prompt2(ref valueName, ref valueCurrentDir, ref valueDesDir);
                if (result == DialogResult.OK)
                {
                    sysController.Name = valueName;
                    sysController.CurrentDirectory = valueCurrentDir;
                    sysController.DesiredDirectory = valueDesDir;
                    sysController.EditMethod();
                    reBindDataGridView();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void setViewModelStrings()
        {
            sysController.Name = textBoxName.Text;
            sysController.CurrentDirectory = textBoxStart.Text;
            sysController.DesiredDirectory = textBoxEnd.Text;
        }
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            var box = (ListBox) sender;
            var indix = box.SelectedIndex;
            listBoxName.SelectedIndex = indix;
            if (listBox4TotalSize.Items.Count > 0)
            {
                listBoxCurrentDir.SelectedIndex = indix;
                listBoxNumberOfFiles.SelectedIndex = indix;
                listBoxCurrentDir.SelectedIndex = indix;
            }
            sysController.CurrentSelectedItem = listBoxName.SelectedItem as AdvancedDirectory;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!sysController.Saved)
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    var result = MessageBox.Show("Unsaved changes has been made. \nSave before closing?", "Save", MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.Yes)
                        sysController.SavePrompt();
                    if (result == DialogResult.Cancel)
                        e.Cancel = true;
                }

            }

            if (e.CloseReason == CloseReason.WindowsShutDown)
                sysController.SavePrompt();
        }


        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = new FolderBrowserDialog();
            result.ShowDialog();
            sysController.NewCollection(result.SelectedPath);
            reBindDataGridView();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = new OpenFileDialog();
            result.ShowDialog();            
            string success = "";
            sysController.OpenFileDialog(result.FileName, ref success);
            if (success.Contains("Something"))
                MessageBox.Show(success);
            reBindDataGridView();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sysController.SavePrompt();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = new SaveFileDialog();
            result.ShowDialog();
            sysController.SaveFile(result.FileName);
            reBindDataGridView();
        }
    }
}

