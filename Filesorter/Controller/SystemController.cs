﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using Filesorter.Model;

namespace Filesorter.Controller
{
    class SystemController
    {
        public string CurrentDirectory { get; set; }
        public string DesiredDirectory { get; set; }
        public AdvancedDirectory CurrentSelectedItem { get; set; }
        public string Name { get; set; }

        private static string lastPath;
        /// <summary>
        /// Should be current CollectionItemCatalog.
        /// </summary>

        private CollectionItemCatalog collectionItemCatalog;

        public SystemController()
        {
            var success = "";
            collectionItemCatalog = CollectionItemCatalog.GetCollectionItemCatalog(lastPath, ref success);
            lastPath = collectionItemCatalog.LastPath;
            collectionItemCatalog.Recalculate();
            Saved = true;
        }

        public ObservableCollection<AdvancedDirectory> CollectionCatalog
        {
            get
            {                
                return collectionItemCatalog.CollectionList;
            }
        }

        public void AddMethod()
        {
            collectionItemCatalog.AddCollectionItem(new AdvancedDirectory(CurrentDirectory, DesiredDirectory, Name));
            collectionItemCatalog.Recalculate();
            Saved = false;
        }

        public void EditMethod()
        {
            if (CurrentSelectedItem != null)
            {
                if (!string.IsNullOrWhiteSpace(Name))
                    CurrentSelectedItem.CollectionName = Name;
                if (!string.IsNullOrWhiteSpace(CurrentDirectory))
                    CurrentSelectedItem.CurrentDirectory = CurrentDirectory;
                if (!string.IsNullOrWhiteSpace(DesiredDirectory))
                    CurrentSelectedItem.DesiredDirectory = DesiredDirectory;
                Saved = false;
            }
            else
            {
                throw new Exception("No selected item was found!");
            }
            collectionItemCatalog.Recalculate();
        }

        public void DeleteMethod()
        {
            if (CurrentSelectedItem != null)
            {
                collectionItemCatalog.RemoveCollectionItem(CurrentSelectedItem);
                Saved = false;
            }
            else
            {
                throw new Exception("No selected item was found!");
            }
            collectionItemCatalog.Recalculate();
        }

        public void SortMethod()
        {
            foreach (var dir in CollectionCatalog)
            {               
                MoveDirectoryFile.Move(dir);
                collectionItemCatalog.Recalculate();
            }
            Saved = false;
        }

        public void SavePrompt()
        {
            collectionItemCatalog.SaveCurrentCollection(lastPath, collectionItemCatalog);
            collectionItemCatalog.Recalculate();
            Saved = true;
        }

        public void OpenFileDialog(string choosenPath, ref string success)
        {
            collectionItemCatalog = collectionItemCatalog.OpenCollectionItemCatalogFile(choosenPath, ref success);
            collectionItemCatalog.LastPath = choosenPath;
            collectionItemCatalog.Recalculate();
            Saved = true;
        }

        private static SystemController viewModel;
        public static SystemController GetViewModel
        {
            get
            {
                if (viewModel != null)
                    return viewModel;

                return viewModel = new SystemController();
            }
        }

        public bool Saved { get; internal set; }

        internal void NewCollection(string selectedPath)
        {
            SaveFileDialog dialog = new SaveFileDialog
            {
                DefaultExt = ".xml",
                InitialDirectory = Application.StartupPath,
                AddExtension = true
            };
            dialog.ShowDialog();
            collectionItemCatalog = new CollectionItemCatalog(new ObservableCollection<AdvancedDirectory>());
            collectionItemCatalog.SaveCurrentCollection(dialog.FileName, collectionItemCatalog);
            collectionItemCatalog.Recalculate();
            Saved = true;
        }

        public void SaveFile(string fileName)
        {
            collectionItemCatalog.SaveCurrentCollection(fileName, collectionItemCatalog);
            collectionItemCatalog.Recalculate();
            Saved = true;
        }
    }
}
