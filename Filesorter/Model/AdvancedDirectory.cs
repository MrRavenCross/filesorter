﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Filesorter.Model
{
    [Serializable]

    public class AdvancedDirectory
    {
        [XmlIgnore]
        private FileInfo[] directoryFiles;
        [XmlIgnore]
        private DirectoryInfo[] mainSubDirs;
        private string collectionName;
        private string desiredDirectory;
        private string currentDirectory;
        private long totalSize;       
        private int numberOfFiles;      

        /// <summary>
        /// Provides properties and methods for seeing what files is present in the directory, 
        /// </summary>
        /// <param name="currentDirectory">Is the current directory the files/subdirectories are located</param>
        /// <param name="desiredDirectory">Is the directory that the files/subdirectories is going to be moved to</param>
        /// <param name="collectionName">Is the name of the collection</param>
        public AdvancedDirectory(string currentDirectory, string desiredDirectory, string collectionName)
        {
            CollectionName = collectionName;
            CurrentDirectory = currentDirectory;
            DesiredDirectory = desiredDirectory + @"\" + CollectionName;
            totalSize = 0;
            numberOfFiles = 0;
            directoryInfo();
        }
        /// <summary>
        /// Call this to get total size of the directory in KB.
        /// </summary>
        public long TotalSize
        {
            get
            {
                totalSize = 0;
                if (directoryFiles != null && directoryFiles.Any())
                {
                    foreach (var file in directoryFiles)
                    {
                        totalSize += file.Length;
                    }

                }
                if (mainSubDirs != null && mainSubDirs.Any())
                {
                    foreach (var subDir in mainSubDirs)
                    {
                        var files = subDir.GetFiles();
                        foreach (var file in files)
                        {
                            
                            totalSize += file.Length;
                        }
                    }
                }
                return totalSize/1000;
            }
        }
        /// <summary>
        /// Call this to get the number of files in the directory.
        /// </summary>
        public int NumberOfFiles
        {
            get
            {
                numberOfFiles = 0;
                if (directoryFiles != null && directoryFiles.Any())
                {
                    numberOfFiles += directoryFiles.Length;
                }
                if (mainSubDirs != null && mainSubDirs.Any())
                {
                    foreach (var subDir in mainSubDirs)
                    {
                        var files = subDir.GetFiles();
                        numberOfFiles += files.Length;
                    }
                }
                return numberOfFiles;
            }
        }
        private AdvancedDirectory()
        {
            //This is here because XMLSerializer is retarded.
        }

        /// <summary>
        /// Call this to get the directory's size to Megabytes from bytes
        /// </summary>
        /// <returns>Size in Megabytes instead of bytes</returns>
        public long ToMegabytes
        {
            get
            {
                directoryInfo();
                return TotalSize / 1000;
            }
        }
        /// <summary>
        /// Automatically gets the appropriate size and surfix for it.
        /// </summary>
        public string GetSizeAppropriate
        {
            
            get
            {
                double appropriateSize = TotalSize;
                string sizeText = " KB";
                if (appropriateSize >= 1000)
                {
                    sizeText = " MB";
                    appropriateSize /= 1000;
                }
                if (appropriateSize >= 1000)
                {
                    sizeText = " GB";
                    appropriateSize /= 1000;
                }
                if (appropriateSize >= 1000)
                {
                    sizeText = " PB";
                    appropriateSize /= 1000;
                }

                if(appropriateSize.ToString(CultureInfo.CurrentCulture).Length >= 4)
                    return appropriateSize.ToString(CultureInfo.CurrentCulture).Remove(4) + sizeText;
                return appropriateSize.ToString(CultureInfo.CurrentCulture) + sizeText;
            }
        }
        /// <summary>
        /// Call this to change the collection name
        /// </summary>
        public string CollectionName
        {
            get { return collectionName; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    collectionName = value;
                else
                    throw new ArgumentException("The name cannot be null or empty");
            }
        }
        /// <summary>
        /// Call this to change the path of the desired location of the directory
        /// </summary>
        public string DesiredDirectory
        {
            get { return desiredDirectory; }
            set
            {               
                if (value.Contains(@"\Windows\System") || !value.Contains(":/") && !value.Contains(@":\") || value.Contains("/Windows/System"))
                {
                    throw new IOException("You need to choose a valid folder. " + value + " is not valid.");
                }
                if (!string.IsNullOrWhiteSpace(value))
                {
                    desiredDirectory = value;
                    desiredDirectory = desiredDirectory.Replace("/", @"\");
                }
                
                else
                    throw new ArgumentException("Desired folder cannot be null or empty");
            }
        }

        /// <summary>
        /// Call this to change the path of the starting location of the directory
        /// </summary>
        public string CurrentDirectory
        {
            get { return currentDirectory; }
            set
            {
                if (value.Contains(@"\Windows\System") || !value.Contains(":/") && !value.Contains(@":\") || value.Contains("/Windows/System"))
                {
                    throw new IOException("You need to choose a valid folder. \n" + value + " is not valid.");
                }

                if (!string.IsNullOrWhiteSpace(value))
                {
                    currentDirectory = value;
                    currentDirectory = currentDirectory.Replace("/", @"\");
                }
                else
                    throw new ArgumentException("Starting folder cannot be null or empty");
            }
        }

        internal void Recalculate()
        {
            directoryInfo();
            var h = TotalSize;
            var j = NumberOfFiles;
        }

        /// <summary>
        /// Call this to get the Files from the Advanced Directory.
        /// </summary>
        [XmlIgnore]
        public FileInfo[] DirectoryFiles
        {
            get
            {
                directoryInfo();
                return directoryFiles;
            }
            private set { directoryFiles = value; }
        }
        /// <summary>
        /// Call this to get the Sub directories from the main directory.
        /// </summary>
        [XmlIgnore]
        public DirectoryInfo[] MainSubDirs
        {
            get
            {
                directoryInfo();
                return mainSubDirs;
            }
            private set { mainSubDirs = value; }
        }

        private void directoryInfo()
        {
            if (!Directory.Exists(DesiredDirectory))
                Directory.CreateDirectory(DesiredDirectory);


            DirectoryInfo di = new DirectoryInfo(DesiredDirectory);
            if (Directory.EnumerateFiles(DesiredDirectory).Any())
            {
                DirectoryFiles = di.GetFiles();
            }
            if (Directory.EnumerateDirectories(DesiredDirectory).Any())
            {
                MainSubDirs = di.GetDirectories();
            }
        }

        public override string ToString()
        {
            return "The directory " + CollectionName + "'s size is: " + ToMegabytes + "MB " + NumberOfFiles;
        }

        /// <summary>
        /// Call this for every information available on the directory
        /// </summary>
        /// <returns>a string with all available information</returns>
        public string ToFullString()
        {
            string g = ToString();

            g += "\nFiles present in this directory is: \n";
            return DirectoryFiles.Aggregate(g, (current, file) => current + ("Name: " + file.Name + "Size in Megabytes: " + file.Length / 1000 + "\n"));
        }
    }
}
