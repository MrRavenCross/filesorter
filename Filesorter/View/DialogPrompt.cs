﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Filesorter.Properties;

namespace Filesorter.View
{
    class DialogPrompt
    {
        public static DialogResult Prompt(string title, string promptText, ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = Resources.DialogPrompt_Prompt_OK;
            buttonCancel.Text = Resources.DialogPrompt_Prompt_Cancel;
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        static FolderBrowserDialog folderBrowserDialogStart;
        static FolderBrowserDialog folderBrowserDialogEnd;
        static TextBox textBoxStart;
        static TextBox textBoxEnd;

        public static DialogResult Prompt2(ref string value1, ref string value2, ref string value3)
        {
            Form form = new Form();
            var label1 = new System.Windows.Forms.Label();
            var labelDesc2 = new System.Windows.Forms.Label();
            var labelDesc1 = new System.Windows.Forms.Label();
            var buttonEnd = new System.Windows.Forms.Button();
            var buttonStart = new System.Windows.Forms.Button();
            var label3 = new System.Windows.Forms.Label();
            var label2 = new System.Windows.Forms.Label();
            var labelName = new System.Windows.Forms.Label();
            var textBoxName = new System.Windows.Forms.TextBox();
            textBoxStart = new System.Windows.Forms.TextBox();
            textBoxEnd = new System.Windows.Forms.TextBox();
            var button1 = new System.Windows.Forms.Button();
            var button2 = new System.Windows.Forms.Button();
            folderBrowserDialogStart = new System.Windows.Forms.FolderBrowserDialog();
            folderBrowserDialogEnd = new System.Windows.Forms.FolderBrowserDialog();
            form.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(13, 201);
            label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label1.MaximumSize = new System.Drawing.Size(300, 0);
            label1.Name = "label1";
            label1.Size = new Size(298, 26);
            label1.TabIndex = 30;
            label1.Text = Resources.DialogPrompt_Prompt2_;
            // 
            // labelDesc2
            // 
            labelDesc2.AutoSize = true;
            labelDesc2.Location = new System.Drawing.Point(13, 85);
            labelDesc2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            labelDesc2.MaximumSize = new System.Drawing.Size(275, 70);
            labelDesc2.Name = "labelDesc2";
            labelDesc2.Size = new System.Drawing.Size(271, 65);
            labelDesc2.TabIndex = 29;
            labelDesc2.Text = "Choose the path where all the files or directories are located. \r\nThen choose the" +
    " path where you want the collection or directories to be moved.\r\n\r\n";
            // 
            // labelDesc1
            // 
            labelDesc1.AutoSize = true;
            labelDesc1.Location = new System.Drawing.Point(13, 10);
            labelDesc1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            labelDesc1.MaximumSize = new System.Drawing.Size(225, 0);
            labelDesc1.Name = "labelDesc1";
            labelDesc1.Size = new System.Drawing.Size(218, 26);
            labelDesc1.TabIndex = 28;
            labelDesc1.Text = "Write the name of the collection you want to automatically sort";
            // 
            // buttonEnd
            // 
            buttonEnd.Location = new System.Drawing.Point(248, 252);
            buttonEnd.Margin = new System.Windows.Forms.Padding(4);
            buttonEnd.Name = "buttonEnd";
            buttonEnd.Size = new System.Drawing.Size(63, 25);
            buttonEnd.TabIndex = 27;
            buttonEnd.Text = "Open";
            buttonEnd.UseVisualStyleBackColor = false;
            buttonEnd.Click += ButtonEndOnClick;
            // 
            // buttonStart
            // 
            buttonStart.Location = new System.Drawing.Point(248, 171);
            buttonStart.Margin = new System.Windows.Forms.Padding(4);
            buttonStart.Name = "buttonStart";
            buttonStart.Size = new System.Drawing.Size(63, 25);
            buttonStart.TabIndex = 26;
            buttonStart.Text = "Open";
            buttonStart.UseVisualStyleBackColor = false;
            buttonStart.Click += ButtonStartOnClick;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(13, 236);
            label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(106, 13);
            label3.TabIndex = 25;
            label3.Text = "Edit End Destination:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(13, 155);
            label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(109, 13);
            label2.TabIndex = 24;
            label2.Text = "Edit Start Destination:";
            // 
            // labelName
            // 
            labelName.AutoSize = true;
            labelName.Location = new System.Drawing.Point(11, 47);
            labelName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            labelName.Name = "labelName";
            labelName.Size = new System.Drawing.Size(59, 13);
            labelName.TabIndex = 23;
            labelName.Text = "Edit Name:";
            // 
            // textBoxName
            // 
            textBoxName.Location = new System.Drawing.Point(72, 44);
            textBoxName.Margin = new System.Windows.Forms.Padding(4);
            textBoxName.Name = "textBoxName";
            textBoxName.Size = new System.Drawing.Size(199, 20);
            textBoxName.TabIndex = 22;
            // 
            // textBoxStart
            // 
            textBoxStart.Location = new System.Drawing.Point(20, 174);
            textBoxStart.Margin = new System.Windows.Forms.Padding(4);
            textBoxStart.Name = "textBoxStart";
            textBoxStart.Size = new System.Drawing.Size(211, 20);
            textBoxStart.TabIndex = 21;
            // 
            // textBoxEnd
            // 
            textBoxEnd.Location = new System.Drawing.Point(20, 255);
            textBoxEnd.Margin = new System.Windows.Forms.Padding(4);
            textBoxEnd.Name = "textBoxEnd";
            textBoxEnd.Size = new System.Drawing.Size(211, 20);
            textBoxEnd.TabIndex = 20;
            // 
            // button1
            // 
            button1.Location = new System.Drawing.Point(16, 291);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(75, 23);
            button1.TabIndex = 31;
            button1.Text = Resources.DialogPrompt_Prompt_OK;
            button1.UseVisualStyleBackColor = true;
            button1.DialogResult = DialogResult.OK;
            // 
            // button2
            // 
            button2.Location = new System.Drawing.Point(236, 291);
            button2.Name = "button2";
            button2.Size = new System.Drawing.Size(75, 23);
            button2.TabIndex = 32;
            button2.Text = Resources.DialogPrompt_Prompt_Cancel;
            button2.UseVisualStyleBackColor = true;
            button2.DialogResult = DialogResult.Cancel;
            // 
            // Form2
            // 
            form.AcceptButton = button1;
            form.CancelButton = button2;
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MaximizeBox = false;
            form.MinimizeBox = false;
            form.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            form.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            form.ClientSize = new System.Drawing.Size(319, 324);
            form.Controls.Add(button2);
            form.Controls.Add(button1);
            form.Controls.Add(label1);
            form.Controls.Add(labelDesc2);
            form.Controls.Add(labelDesc1);
            form.Controls.Add(buttonEnd);
            form.Controls.Add(buttonStart);
            form.Controls.Add(label3);
            form.Controls.Add(label2);
            form.Controls.Add(labelName);
            form.Controls.Add(textBoxName);
            form.Controls.Add(textBoxStart);
            form.Controls.Add(textBoxEnd);
            form.Name = "FormPrompt";
            form.Text = "Edit fields";
            form.ResumeLayout(false);
            form.PerformLayout();
            //
            //logic
            //
            DialogResult result = form.ShowDialog();
            if (result == DialogResult.OK)
            {
                value1 = textBoxName.Text;
                value2 = textBoxStart.Text;
                value3 = textBoxEnd.Text;
            }
            return result;
        }

        private static void ButtonStartOnClick(object sender, EventArgs eventArgs)
        {
            folderBrowserDialogStart.ShowDialog();
            textBoxStart.Text = folderBrowserDialogStart.SelectedPath;
        }

        private static void ButtonEndOnClick(object sender, EventArgs eventArgs)
        {
            folderBrowserDialogEnd.ShowDialog();
            textBoxEnd.Text = folderBrowserDialogEnd.SelectedPath;
        }
    }
}
