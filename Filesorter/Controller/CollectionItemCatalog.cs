﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Filesorter.Model;

namespace Filesorter.Controller
{
    [Serializable]
    public class CollectionItemCatalog
    {
        public ObservableCollection<AdvancedDirectory> CollectionList { get; set; }
        [XmlIgnore]
        public string LastPath { get; set; }
        /// <summary>
        /// Is a class responsible for all the AdvancedDirectories.
        /// </summary>
        /// <param name="collectionItemsList">Is an ObservableCollection that can contain AdvancedDirectories</param>
        public CollectionItemCatalog(ObservableCollection<AdvancedDirectory> collectionItemsList)
        {
            CollectionList = collectionItemsList;
        }

        private CollectionItemCatalog()
        {
            //This is here because the XMLSerializer is retarded
        }

        /// <summary>
        /// Opens the xml file at the specified path.
        /// If no xml file is at the specified path, a new collection will be made.
        /// </summary>
        /// <param name="path">Is where the xml file is located</param>
        /// <param name="success">A bool that tells if the deserialization was completed successfully</param>
        /// <returns> a CollectionItemCatalog</returns>
        public static CollectionItemCatalog GetCollectionItemCatalog(string path, ref string success)
        {
            CollectionItemCatalog collectionItemCatalog;
            try
            {                
                collectionItemCatalog = deSerialize(path, ref success);                                   
            }
            catch (Exception)
            {
                collectionItemCatalog = new CollectionItemCatalog(new ObservableCollection<AdvancedDirectory>());
            }
            return collectionItemCatalog;
        }


        /// <summary>
        /// Call this to add a collectionItem to the Catalog
        /// </summary>
        /// <param name="collectionItem">Is the collectionItem to be added</param>
        /// <returns>a bool based on the success on the operation</returns>
        public bool AddCollectionItem(AdvancedDirectory collectionItem)
        {
            bool g = false;
            if (!CollectionList.Contains(collectionItem))
            {
                CollectionList.Add(collectionItem);

                g = true;
            }
            return g;
        }
        /// <summary>
        /// Call this to edit the desired directory path.
        /// </summary>
        /// <param name="dir">the directory that is going to have it's path changed</param>
        /// <param name="newDesiredDirectory">is the new desired directory path</param>
        /// <returns>return true if successfull, and false if the desired directory does not exist</returns>
        public bool EditDesiredDirectory(AdvancedDirectory dir, string newDesiredDirectory)
        {
            if (Directory.Exists(newDesiredDirectory))
            {
                dir.DesiredDirectory = newDesiredDirectory;
                return true;
            }
            return false;
        }
        /// <summary>
        /// Call this to remove an item from the CollectionItemCatalog
        /// </summary>
        /// <param name="currentSelectedItem">Is the item that is going to be removed</param>
        internal void RemoveCollectionItem(AdvancedDirectory currentSelectedItem)
        {
            for (int i = CollectionList.Count - 1; i >= 0 ; i--)
            {
                if (CollectionList[i].CollectionName == currentSelectedItem.CollectionName)
                    CollectionList.RemoveAt(i);
            }                         
        }

        public void Recalculate()
        {
            foreach (var advDir in CollectionList)
            {
                advDir.Recalculate();
            }
        }


        #region Xml serialization


        private static XmlSerializer serializer;
        private static Stream stream;
        /// <summary>
        /// Saves the CollectionItemCatalog.
        /// </summary>
        /// <param name="path"> Is where the file is going to be saved</param>
        /// <param name="collectionItemCatalog">Is the CollectionItemCatalog that is going to be saved</param>
        internal void SaveCurrentCollection(string path, CollectionItemCatalog collectionItemCatalog)
        {
            serialize(path, collectionItemCatalog);
        }

        internal CollectionItemCatalog OpenCollectionItemCatalogFile(string lastPath, ref string success)
        {
            return deSerialize(lastPath, ref success);
        }
        /// <summary>
        /// Needs to be called before all serializations. If no path is provided it will choose the default path.
        /// </summary>
        /// <param name="path">Is the path, where the CollectionItemCatalog will be saved</param>
        private static void myXmlSerializer(string path)
        {
            
            serializer = new XmlSerializer(typeof(CollectionItemCatalog));
            defaultPath = path;
            if (string.IsNullOrWhiteSpace(defaultPath))
            {
                defaultPath = Path.Combine(Application.StartupPath, "DefaultCollectionSave.xml");
            }
        }

        private static string defaultPath;

        /// <summary>
        /// Call this to serialize/save collectionItemCatalog
        /// </summary>
        /// <param name="path">Is the path where the xml file will be saved</param>
        /// <param name="collectionItemCatalog">Is the collectionItemCatalog that is going to be serialized/saved </param>
        /// <returns>returns a bool wether or not the serialization was completed</returns>
        private static void serialize(string path, CollectionItemCatalog collectionItemCatalog)
        {
            try
            {
                myXmlSerializer(path);
                stream = new FileStream(defaultPath, FileMode.Truncate);
                serializer.Serialize(stream, collectionItemCatalog);
            }
            finally
            { stream.Close(); }
            
        }

        /// <summary>
        /// Call this to deserialize/open a collectionItemCatalog xml file
        /// </summary>
        /// <returns>a collectionItemCatalog</returns>
        private static CollectionItemCatalog deSerialize(string path, ref string success)
        {
            if (path != null)
            {
                try
                {
                    myXmlSerializer(path);
                    stream = new FileStream(defaultPath, FileMode.Open);
                    var col = (CollectionItemCatalog) serializer.Deserialize(stream);
                    success = "Success";
                    return col;
                }
                catch (IOException ioE)
                {
                    success = "Something went wrong: " + ioE.Message;
                }
                finally
                {
                    stream.Close();
                }
            }
            else
            {
                throw new NullReferenceException("You can't have an empty path.");
            }
            return null;
        }
#endregion
    }
}
